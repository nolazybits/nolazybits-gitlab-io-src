#!/bin/sh
current_dir=$(dirname $(readlink -f $0))
HUGO_DESTINATION="${HUGO_DESTINATION:=/hugo/output}"
echo "HUGO_WATCH:" $WATCH
echo "HUGO_REFRESH_TIME:" $HUGO_REFRESH_TIME
echo "HUGO_THEME:" $HUGO_THEME
echo "HUGO_BASEURL" $HUGO_BASEURL
echo "ARGS" $@

HUGO=/usr/local/sbin/hugo
echo "Hugo path: $HUGO"

echo -e "\033[0;32mDeploying updates to GitHub...\033[0m"

# Build the project.
echo "Building one time..."
$HUGO -v --source="/hugo/src" --theme="$HUGO_THEME" --destination="$HUGO_DESTINATION" "$@" || exit 1

# Go To Public folder
cd $HUGO_DESTINATION
# Add changes to git.
git add .
# Commit changes.
msg="rebuilding site `date`"
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$msg"

# Push source and build repos.
git push origin master

# Come Back up to the Project Root
cd ..